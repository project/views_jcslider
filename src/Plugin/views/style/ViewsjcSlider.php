<?php

/**
 * @file
 * Contains \Drupal\views_jcslider\Plugin\views\style\ViewsjcSlider.
 */

namespace Drupal\views_jcslider\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
/**
 * Style plugin to render each item into jcSlider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "viewsjcslider",
 *   title = @Translation("Views jcSlider"),
 *   help = @Translation("Integrates jcSlider plugin to views rows."),
 *   theme = "views_jcslider_views",
 *   display_types = {"normal"}
 * )
 */
class ViewsjcSlider extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $settings = _views_jcslider_default_settings();
    foreach ($settings as $k => $v) {
      $options[$k] = array('default' => $v);
    }
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    //animationIn
    $form['animationIn'] = array(
      '#type' => 'select',
      '#options' => array(
        'bounce' => $this->t('bounce'),
        'flash' => $this->t('flash'),
        'pulse' => $this->t('pulse'),
        'rubberBand' => $this->t('rubberBand'),
        'shake' => $this->t('shake'),
        'headShake' => $this->t('headShake'),
        'swing' => $this->t('swing'),
        'tada' => $this->t('tada'),
        'wobble' => $this->t('wobble'),
        'jello' => $this->t('jello'),
        'bounceIn' => $this->t('bounceIn'),
        'bounceInDown' => $this->t('bounceInDown'),
        'bounceInLeft' => $this->t('bounceInLeft'),
        'bounceInRight' => $this->t('bounceInRight'),
        'bounceInUp' => $this->t('bounceInUp'),
        'bounceOut' => $this->t('bounceOut'),
        'bounceOutDown' => $this->t('bounceOutDown'),
        'bounceOutLeft' => $this->t('bounceOutLeft'),
        'bounceOutRight' => $this->t('bounceOutRight'),
        'bounceOutUp' => $this->t('bounceOutUp'),
        'fadeIn' => $this->t('fadeIn'),
        'fadeInDown' => $this->t('fadeInDown'),
        'fadeInDownBig' => $this->t('fadeInDownBig'),
        'fadeInLeft' => $this->t('fadeInLeft'),
        'fadeInLeftBig' => $this->t('fadeInLeftBig'),
        'fadeInRight' => $this->t('fadeInRight'),
        'fadeInRightBig' => $this->t('fadeInRightBig'),
        'fadeInUp' => $this->t('fadeInUp'),
        'fadeInUpBig' => $this->t('fadeInUpBig'),
        'fadeOut' => $this->t('fadeOut'),
        'fadeOutDown' => $this->t('fadeOutDown'),
        'fadeOutDownBig' => $this->t('fadeOutDownBig'),
        'fadeOutLeft' => $this->t('fadeOutLeft'),
        'fadeOutLeftBig' => $this->t('fadeOutLeftBig'),
        'fadeOutRight' => $this->t('fadeOutRight'),
        'fadeOutRightBig' => $this->t('fadeOutRightBig'),
        'fadeOutUp' => $this->t('fadeOutUp'),
        'fadeOutUpBig' => $this->t('fadeOutUpBig'),
        'flipInX' => $this->t('flipInX'),
        'flipInY' => $this->t('flipInY'),
        'flipOutX' => $this->t('flipOutX'),
        'flipOutY' => $this->t('flipOutY'),
        'lightSpeedIn' => $this->t('lightSpeedIn'),
        'lightSpeedOut' => $this->t('lightSpeedOut'),
        'rotateIn' => $this->t('rotateIn'),
        'rotateInDownLeft' => $this->t('rotateInDownLeft'),
        'rotateInDownRight' => $this->t('rotateInDownRight'),
        'rotateInUpLeft' => $this->t('rotateInUpLeft'),
        'rotateInUpRight' => $this->t('rotateInUpRight'),
        'rotateOut' => $this->t('rotateOut'),
        'rotateOutDownLeft' => $this->t('rotateOutDownLeft'),
        'rotateOutDownRight' => $this->t('rotateOutDownRight'),
        'rotateOutUpLeft' => $this->t('rotateOutUpLeft'),
        'rotateOutUpRight' => $this->t('rotateOutUpRight'),
        'hinge' => $this->t('hinge'),
        'jackInTheBox' => $this->t('jackInTheBox'),
        'rollIn' => $this->t('rollIn'),
        'rollOut' => $this->t('rollOut'),
        'zoomIn' => $this->t('zoomIn'),
        'zoomInDown' => $this->t('zoomInDown'),
        'zoomInLeft' => $this->t('zoomInLeft'),
        'zoomInRight' => $this->t('zoomInRight'),
        'zoomInUp' => $this->t('zoomInUp'),
        'zoomOut' => $this->t('zoomOut'),
        'zoomOutDown' => $this->t('zoomOutDown'),
        'zoomOutLeft' => $this->t('zoomOutLeft'),
        'zoomOutRight' => $this->t('zoomOutRight'),
        'zoomOutUp' => $this->t('zoomOutUp'),
        'slideInDown' => $this->t('slideInDown'),
        'slideInLeft' => $this->t('slideInLeft'),
        'slideInRight' => $this->t('slideInRight'),
        'slideInUp' => $this->t('slideInUp'),
        'slideOutDown' => $this->t('slideOutDown'),
        'slideOutLeft' => $this->t('slideOutLeft'),
        'slideOutRight' => $this->t('slideOutRight'),
        'slideOutUp' => $this->t('slideOutUp'),
        'heartBeat' => $this->t('heartBeat'),
      ),
      '#title' => $this->t('Animation In'),
      '#default_value' => $this->options['animationIn'],
      '#description' => $this->t('Animation begins in this style.'),
    );
    //animationOut
    $form['animationOut'] = array(
      '#type' => 'select',
      '#options' => array(
        'bounce' => $this->t('bounce'),
        'flash' => $this->t('flash'),
        'pulse' => $this->t('pulse'),
        'rubberBand' => $this->t('rubberBand'),
        'shake' => $this->t('shake'),
        'headShake' => $this->t('headShake'),
        'swing' => $this->t('swing'),
        'tada' => $this->t('tada'),
        'wobble' => $this->t('wobble'),
        'jello' => $this->t('jello'),
        'bounceIn' => $this->t('bounceIn'),
        'bounceInDown' => $this->t('bounceInDown'),
        'bounceInLeft' => $this->t('bounceInLeft'),
        'bounceInRight' => $this->t('bounceInRight'),
        'bounceInUp' => $this->t('bounceInUp'),
        'bounceOut' => $this->t('bounceOut'),
        'bounceOutDown' => $this->t('bounceOutDown'),
        'bounceOutLeft' => $this->t('bounceOutLeft'),
        'bounceOutRight' => $this->t('bounceOutRight'),
        'bounceOutUp' => $this->t('bounceOutUp'),
        'fadeIn' => $this->t('fadeIn'),
        'fadeInDown' => $this->t('fadeInDown'),
        'fadeInDownBig' => $this->t('fadeInDownBig'),
        'fadeInLeft' => $this->t('fadeInLeft'),
        'fadeInLeftBig' => $this->t('fadeInLeftBig'),
        'fadeInRight' => $this->t('fadeInRight'),
        'fadeInRightBig' => $this->t('fadeInRightBig'),
        'fadeInUp' => $this->t('fadeInUp'),
        'fadeInUpBig' => $this->t('fadeInUpBig'),
        'fadeOut' => $this->t('fadeOut'),
        'fadeOutDown' => $this->t('fadeOutDown'),
        'fadeOutDownBig' => $this->t('fadeOutDownBig'),
        'fadeOutLeft' => $this->t('fadeOutLeft'),
        'fadeOutLeftBig' => $this->t('fadeOutLeftBig'),
        'fadeOutRight' => $this->t('fadeOutRight'),
        'fadeOutRightBig' => $this->t('fadeOutRightBig'),
        'fadeOutUp' => $this->t('fadeOutUp'),
        'fadeOutUpBig' => $this->t('fadeOutUpBig'),
        'flipInX' => $this->t('flipInX'),
        'flipInY' => $this->t('flipInY'),
        'flipOutX' => $this->t('flipOutX'),
        'flipOutY' => $this->t('flipOutY'),
        'lightSpeedIn' => $this->t('lightSpeedIn'),
        'lightSpeedOut' => $this->t('lightSpeedOut'),
        'rotateIn' => $this->t('rotateIn'),
        'rotateInDownLeft' => $this->t('rotateInDownLeft'),
        'rotateInDownRight' => $this->t('rotateInDownRight'),
        'rotateInUpLeft' => $this->t('rotateInUpLeft'),
        'rotateInUpRight' => $this->t('rotateInUpRight'),
        'rotateOut' => $this->t('rotateOut'),
        'rotateOutDownLeft' => $this->t('rotateOutDownLeft'),
        'rotateOutDownRight' => $this->t('rotateOutDownRight'),
        'rotateOutUpLeft' => $this->t('rotateOutUpLeft'),
        'rotateOutUpRight' => $this->t('rotateOutUpRight'),
        'hinge' => $this->t('hinge'),
        'jackInTheBox' => $this->t('jackInTheBox'),
        'rollIn' => $this->t('rollIn'),
        'rollOut' => $this->t('rollOut'),
        'zoomIn' => $this->t('zoomIn'),
        'zoomInDown' => $this->t('zoomInDown'),
        'zoomInLeft' => $this->t('zoomInLeft'),
        'zoomInRight' => $this->t('zoomInRight'),
        'zoomInUp' => $this->t('zoomInUp'),
        'zoomOut' => $this->t('zoomOut'),
        'zoomOutDown' => $this->t('zoomOutDown'),
        'zoomOutLeft' => $this->t('zoomOutLeft'),
        'zoomOutRight' => $this->t('zoomOutRight'),
        'zoomOutUp' => $this->t('zoomOutUp'),
        'slideInDown' => $this->t('slideInDown'),
        'slideInLeft' => $this->t('slideInLeft'),
        'slideInRight' => $this->t('slideInRight'),
        'slideInUp' => $this->t('slideInUp'),
        'slideOutDown' => $this->t('slideOutDown'),
        'slideOutLeft' => $this->t('slideOutLeft'),
        'slideOutRight' => $this->t('slideOutRight'),
        'slideOutUp' => $this->t('slideOutUp'),
        'heartBeat' => $this->t('heartBeat'),
      ),
      '#title' => $this->t('Animation Out'),
      '#default_value' => $this->options['animationOut'],
      '#description' => $this->t('Animation ends in this style.'),
    );
    //stopOnHover
    $form['stopOnHover'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Stop On Hover'),
      '#default_value' => $this->options['stopOnHover'],
    );
    //loop
    $form['loop'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Loop'),
      '#default_value' => $this->options['loop'],
    );
  }

}
