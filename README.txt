Views jcSlider
==============

CONTENTS OF THIS FILE
---------------------
   
 * INTRODUCTION
 * FEATURES
 * REQUIREMENTS
 * INSTALLATION
 * USAGE
 * MAINTAINERS
 * DEMO

INTRODUCTION
------------

Views jcSlider integrates <a href="http://joanclaret.github.io/jcSlider/" target="_blank">jcSlider</a> (a responsive slider jQuery plugin with CSS animations) to Drupal Views as a display style plugin.

FEATURES
--------

  - Responsive
  - Lightweight
  - Multiple effects (more than 75!)
  - Works with html, images... whatever you want to animate
  - Customizable

REQUIREMENTS
------------

1. Download jcSlider plugin - https://github.com/JoanClaret/jcSlider
2. Download animate.min.css (a cross-browser library of CSS animations) - https://github.com/daneden/animate.css
3. Enable Drupal 8 core modules - Field UI, Image, Views, Views UI
4. Enable Drupal 8 contributed module - Libraries API

INSTALLATION
------------

1. Create a libraries directory in the root if not already there i.e. /libraries
2. Extract the downloaded jcSlider plugin and place it in libraries directory 
similar to the path below (all lowercase):
     - /libraries/jcslider/jquery.jcslider.min.js
3. Place the downloaded animate.min.css file in libraries directory 
similar to the path below (all lowercase):
     - /libraries/animate/animate.min.css
4. Install 'Views jcSlider' module

USAGE
-----

In the views display, under 'FORMAT' section, change the style plugin 'Format' to 'Views jcSlider'. Apply and configure it as per requirement.

MAINTAINERS
-----------

Current maintainer:

 * Binu Varghese - https://www.drupal.org/u/binu-varghese
 
DEMO
----

http://joanclaret.github.io/jcSlider/
