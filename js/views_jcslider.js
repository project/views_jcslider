(function ($) {
    Drupal.behaviors.viewsjcslider = {
        attach: function (context, settings) {
            $('.jc-slider', context).each(function () {
                var $this = $(this);
                var $this_settings = $.parseJSON($this.attr('data-settings'));
                $this.jcSlider($this_settings);

            });

        }
    };
})(jQuery);
